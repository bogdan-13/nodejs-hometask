const { fighter } = require('../models/fighter');
const { BadRequestError } = require('../errors/requestErrors')
minDefense = 1;
maxDefense = 10;
maxPower = 100;

const createFighterValid = (req, res, next) => {

  if (
    !(req.body.name &&
      req.body.power &&
      req.body.defense)
  ) {
    throw new BadRequestError('Missing required fighter data');
  }

  if (parseInt(req.body.defense) < minDefense || parseInt(req.body.defense) > maxDefense) {
    throw new BadRequestError('Defense should be from 1 to 10');
  }

  if (parseInt(req.body.power) >= maxPower) {
    throw new BadRequestError('Power should be less than 100');
  }

  if (req.body.id) {
    throw new BadRequestError('id is not allowed in the body of request');
  }

  if (Object.keys(req.body).length > Object.keys(fighter).length - 1) {
    throw new BadRequestError('Request contains extra parameters');
  }
  next();
}

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update
  if (
    !(req.body.name &&
      req.body.power &&
      req.body.defense)
  ) {
    throw new BadRequestError('Missing required fighter data');
  }

  if (parseInt(req.body.defense) < minDefense || parseInt(req.body.defense) > maxDefense) {
    throw new BadRequestError('Defense should be from 1 to 10');
  }

  if (parseInt(req.body.power) >= maxPower) {
    throw new BadRequestError('Power should be less than 100');
  }

  if (req.body.id) {
    throw new BadRequestError('id is not allowed in the body of request');
  }

  if (Object.keys(req.body).length > Object.keys(fighter).length - 1) {
    throw new BadRequestError('Request contains extra parameters');
  }

  next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;