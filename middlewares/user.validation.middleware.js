const { user } = require('../models/user');
const { BadRequestError } = require('../errors/requestErrors')

const validEmailFormat = /^[\w.+\-]+@g(oogle)?mail\.com$/;
const validPhoneFormat = /^\+380\d{9}$/;
const validPasswordFormat = /^.{3,20}$/;

const createUserValid = (req, res, next) => {
  if (
    !(req.body.email &&
      req.body.firstName &&
      req.body.lastName &&
      req.body.phoneNumber &&
      req.body.password)
  ) {
    throw new BadRequestError('Missing required user data');
  }

  if (!validEmailFormat.test(req.body.email)) {
    throw new BadRequestError('Wrong email format');
  }

  if (!validPhoneFormat.test(req.body.phoneNumber)) {
    throw new BadRequestError('Wrong phone number format');
  }

  if (!validPasswordFormat.test(req.body.password)) {
    throw new BadRequestError('Password is too short');
  }

  if (req.body.id) {
    throw new BadRequestError('id is not allowed in the body of request');
  }

  if (Object.keys(req.body).length > Object.keys(user).length - 1) {
    throw new BadRequestError('Request contains extra parameters');
  }

  next();
}

const updateUserValid = (req, res, next) => {
  if (
    !(req.body.email &&
      req.body.firstName &&
      req.body.lastName &&
      req.body.phoneNumber &&
      req.body.password)
  ) {
    throw new BadRequestError('Missing required user data');
  }

  if (!validEmailFormat.test(req.body.email)) {
    throw new BadRequestError('Wrong email format');
  }

  if (!validPhoneFormat.test(req.body.phoneNumber)) {
    throw new BadRequestError('Wrong phone number format');
  }

  if (!validPasswordFormat.test(req.body.password)) {
    throw new BadRequestError('Password is too short');
  }

  if (req.body.id) {
    throw new BadRequestError('id is not allowed in the body of request');
  }

  if (Object.keys(req.body).length > Object.keys(user).length - 1)  {
    throw new BadRequestError('Request contains extra parameters');
  }

  next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;