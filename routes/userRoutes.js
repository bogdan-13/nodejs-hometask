const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
router.get('/', getUsers, responseMiddleware);

router.get('/:id', getUser, responseMiddleware);

router.post('/', createUserValid, createUser, responseMiddleware);

router.put('/:id', updateUserValid, updateUser, responseMiddleware);

router.delete('/:id', deleteUser, responseMiddleware);

function getUsers(req, res, next) {
  try {
    res.data = UserService.getUsers();
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}

function getUser(req, res, next) {
  try {
    const id = req.params.id;
    res.data = UserService.getUser(id);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}

function createUser(req, res, next) {
  if (res.err) {
    return next();
  }
  try {
    const data = req.body;
    res.data = UserService.createUser(data);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}

function updateUser(req, res, next) {
  if (res.err) {
    return next();
  }
  try {
    const id = req.params.id;
    const data = req.body;
    res.data = UserService.updateUser(id, data);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}

function deleteUser(req, res, next) {
  try {
    const id = req.params.id;
    res.data = UserService.deleteUser(id);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}

module.exports = router;