const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', getFighters, responseMiddleware);

router.get('/:id', getFighter, responseMiddleware);

router.post(
  '/',
  createFighterValid,
  createFighter,
  responseMiddleware,
);

router.put(
  '/:id',
  updateFighterValid,
  updateFighter,
  responseMiddleware,
);

router.delete('/:id', deleteFighter, responseMiddleware);

function getFighters(req, res, next) {
  try {
    res.data = FighterService.getFighters();
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}

function getFighter(req, res, next) {
  try {
    const id = req.params.id;
    res.data = FighterService.getFighter(id);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}

function createFighter(req, res, next) {
  if (res.err) {
    return next();
  }
  try {
    const data = req.body;
    res.data = FighterService.createFighter(data);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}

function updateFighter(req, res, next) {
  if (res.err) {
    return next();
  }
  try {
    const id = req.params.id;
    const data = req.body;
    res.data = FighterService.updateFighter(id, data);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}

function deleteFighter(req, res, next) {
  try {
    const id = req.params.id;
    res.data = FighterService.deleteFighter(id);
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
}

module.exports = router;