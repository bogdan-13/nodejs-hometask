const { UserRepository } = require('../repositories/userRepository');
const { BadRequestError, NotFoundError } = require('../errors/requestErrors');

class UserService {
  getUsers() {
    const users = UserRepository.getAll();
    if (!users) {
      throw new NotFoundError('Users not found');
    }
    return users;
  }

  getUser(id) {
    const user = this.search({ id });
    if (!user) {
      throw new NotFoundError('User not found');
    }
    return user;
  }

  createUser(userData) {
    const { email, phoneNumber } = userData;
    this.checkEmail(email);
    this.isExistPhone(phoneNumber);
    return UserRepository.create(userData);
  }

  updateUser(id, userData) {
    const userToUpdate = this.search({ id });
    if (!userToUpdate) {
      throw new NotFoundError(
        'User cannot be updated, because user not found',
      );
    }
    const { email, phoneNumber } = userData;
    this.checkEmail(email, id);
    this.isExistPhone(phoneNumber, id);
    return UserRepository.update(id, userData);
  }

  deleteUser(id) {
    const userToDelete = this.search({ id });
    if (!userToDelete) {
      throw new NotFoundError(
        'User cannot be deleted, because user not found',
      );
    }
    return UserRepository.delete(id);
  }

  checkEmail(email, id = null) {
    const user = this.search({ email });
    this.isRegistered(user, 'email', id);
  }

  isExistPhone(phoneNumber, id = null) {
    const user = this.search({ phoneNumber });
    this.isRegistered(user, 'phoneNumber', id);
  }

  isRegistered(user, field, id) {
    if (!user) {
      return;
    }
    const isAnotherId = !id ? true : user.id !== id;
    if (isAnotherId) {
      throw new BadRequestError(
        `User with this ${field} is already registered`,
      );
    }
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if(!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();