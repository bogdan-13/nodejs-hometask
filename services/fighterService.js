const {FighterRepository} = require('../repositories/fighterRepository');
const {BadRequestError, NotFoundError} = require('../errors/requestErrors');

class FighterService {
  getFighters() {
    const fighters = FighterRepository.getAll();
    if (!fighters) {
      throw new NotFoundError('Fighters not found');
    }
    return fighters;
  }

  getFighter(id) {
    const fighter = this.search({id});
    if (!fighter) {
      throw new NotFoundError('Fighter not found');
    }
    return fighter;
  }

  createFighter(data) {
    this.checkupName(data.name);
    return FighterRepository.create(data);
  }

  updateFighter(id, data) {
    const fighterToUpdate = this.search({id});
    if (!fighterToUpdate) {
      throw new NotFoundError(
        'Fighter cannot be updated, because fighter not found',
      );
    }
    this.checkupName(data.name, id);
    return FighterRepository.update(id, data);
  }

  deleteFighter(id) {
    const fighterToDelete = this.search({id});
    if (!fighterToDelete) {
      throw new NotFoundError(
        'Fighter cannot be deleted, because fighter not found',
      );
    }
    return FighterRepository.delete(id);
  }

  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  checkupName(name, id = null) {
    const fighter = this.search({name});
    if (!fighter) {
      return;
    }
    const isAnotherId = !id ? true : fighter.id !== id;
    if (isAnotherId) {
      throw new BadRequestError('Fighter with this name has already been created.',
      );
    }
  }
}

module.exports = new FighterService();